import os
import nltk
from nltk.corpus import stopwords
from natasha import Doc, Segmenter, MorphVocab, NewsMorphTagger, NewsEmbedding

# Скачиваем стоп-слова через NLTK и создаем набор для быстрого доступа
nltk.download('stopwords')
russian_stopwords = set(stopwords.words('russian'))

# Инициализируем компоненты Natasha
segmenter = Segmenter()
morph_vocab = MorphVocab()
emb = NewsEmbedding()
morph_tagger = NewsMorphTagger(emb)


# Функция для обработки параграфа
def process_paragraph(paragraph):
    doc = Doc(paragraph)
    doc.segment(segmenter)
    doc.tag_morph(morph_tagger)

    # Применяем лемматизацию и фильтрацию стоп-слов и коротких слов
    lemmatized_tokens = []
    for token in doc.tokens:
        token.lemmatize(morph_vocab)
        if token.lemma.lower() not in russian_stopwords and 3 <= len(token.text) < 20:
            lemmatized_tokens.append(token.lemma)

    return ' '.join(lemmatized_tokens)


# Функция для обработки текстовых файлов
def process_text_files(pages_dir, tokens_dir):
    if not os.path.exists(tokens_dir):
        os.makedirs(tokens_dir)

    for file_name in os.listdir(pages_dir):
        if file_name.endswith('.txt'):
            page_path = os.path.join(pages_dir, file_name)
            token_path = os.path.join(tokens_dir, file_name)

            # Обработка текста
            with open(page_path, 'r', encoding='utf-8') as f:
                paragraphs = f.read().split('\n')
                processed_paragraphs = [process_paragraph(paragraph) for paragraph in paragraphs]
                processed_text = '\n'.join(processed_paragraphs)

            # Сохранение текста
            with open(token_path, 'w', encoding='utf-8') as f:
                f.write(processed_text)


if __name__ == "__main__":
    pages_dir = '../1/pages'
    tokens_dir = 'tokens'

    process_text_files(pages_dir, tokens_dir)
